﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Węgiel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Abonamenty()
        {
            return View();
        }
        public ActionResult Info()
        {
            return View();
        }
        public ActionResult Lokalizacje()
        {
            return View();
        }
        public ActionResult Oferta()
        {
            return View();
        }
        public ActionResult oFirmie()
        {
            return View();
        }
        public ActionResult Transport()
        {
            return View();
        }
        public ActionResult Kontakt()
        {
            return View();
        }
    }
}